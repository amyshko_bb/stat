CLI - Kartina TV
================

Requirements
------------

PHP needs to be a minimum version of PHP 7.0.9.

Installation
------------

Download the `clik.phar` file and store it somewhere on your computer.

Usage
-----
The ``report:monthly:vod`` command tries to send monthly vod report:

```bash
$ php clik.phar report:monthly:vod --receivers=<email>
```
or
```bash
$ php clik.phar report:monthly:vod -r <email> <email> <email>
```

Configuration options
---------------------

`clik.yaml` required file with configuration. Must find in the directory with `clik.phar`

```yaml
databases:
    statistic:
        url: 'mysql://user:pass@nstatsdb.kartina.tv:3306/nstats'
    middleware:
        url: 'mysql://user:pass@donut.kartina.tv:3306/mware'

mailer:
    url: 'smtp://localhost:25/?timeout=60'

```
Build phar
----------
Run

```bash
$ sh dev-tools/build
```
