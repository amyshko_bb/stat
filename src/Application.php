<?php

namespace Clik;

use Clik\Command\VodCommand;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Command\HelpCommand;
use Symfony\Component\Console\Command\ListCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Application extends ConsoleApplication
{
    const VERSION = '1.0.0-DEV';

    public function __construct()
    {
        error_reporting(-1);
        parent::__construct('A cli tool to <comment>Kartina TV</comment>.', self::VERSION);
        $this->addCommands([
            new VodCommand(),
        ]);
    }

    public function doRun(InputInterface $input, OutputInterface $output)
    {
        if (false === $input->hasParameterOption(['--help', '-h']) && null !== $input->getFirstArgument()) {
            $output->writeln($this->getLongVersion());
            $output->writeln('');
        }

        return parent::doRun($input, $output);
    }

    protected function getDefaultCommands()
    {
        return [new ListCommand(), new HelpCommand()];
    }
}
