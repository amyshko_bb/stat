<?php

namespace Clik\Exception;

class InvalidConfigurationException extends \InvalidArgumentException
{
}
