<?php

namespace Clik\Repository;

use Clik\Model\Period;

final class NstatsRepository extends AbstractRepository
{
    public function countVod(Period $period, array $ids = null)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('vod_log', 'v')
            ->where('dt BETWEEN :start AND :end')
            ->setParameters(['start' => $period->getStart(), 'end' => $period->getEnd()]);
        if (null !== $ids && [] !== $ids) {
            $queryBuilder->andWhere($queryBuilder->expr()->notIn(
                'v.fileid',
                $ids
            ));
        }

        return $queryBuilder
            ->execute()
            ->rowCount();
    }

    public function countVodAdvert(Period $period, array $ids = null)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('vod_advert_log', 'v')
            ->where('dt BETWEEN :start AND :end')
            ->setParameters(['start' => $period->getStart(), 'end' => $period->getEnd()]);
        if (null !== $ids && [] !== $ids) {
            $queryBuilder->andWhere($queryBuilder->expr()->notIn(
                'v.id_film',
                $ids
            ));
        }

        return $queryBuilder
            ->execute()
            ->rowCount();
    }
}
