<?php

namespace Clik\Repository;

final class MiddlewareRepository extends AbstractRepository
{
    public function findFilmIdsByCategory(string $category)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('vfg.id_films')
            ->from('vod_films_genre', 'vfg')
            ->innerJoin('vfg', 'vod_genre', 'vg', 'vfg.id_genre = vg.id')
            ->where('vg.name = :category')
            ->setParameter('category', $category);
        $result = $queryBuilder->execute()->fetchAll();

        return array_map(function ($value) {
            return $value['id_films'];
        }, $result);
    }
}
