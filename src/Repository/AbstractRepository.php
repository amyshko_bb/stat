<?php

namespace Clik\Repository;

use Clik\Exception\InvalidConfigurationException;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;

abstract class AbstractRepository
{
    /**
     * @var Connection
     */
    protected $connection;

    public function __construct(array $params)
    {
//        if (!isset($params['url']) || !filter_var($params['url'], FILTER_VALIDATE_URL)) {
//            throw new InvalidConfigurationException('String is not a valid URL.');
//        }

        $this->connection = DriverManager::getConnection($params, new Configuration());
    }
}
