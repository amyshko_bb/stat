<?php

declare(strict_types=1);

namespace Clik\Command;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Yaml\Yaml;

abstract class AbstractCommand extends Command
{
    /**
     * @var array
     */
    protected $config;

    protected function configure()
    {
        $this->addOption('configuration', 'c', InputOption::VALUE_REQUIRED, 'The configuration file to load');
    }

    protected function loadConfig(InputInterface $input)
    {
        $configFile = $input->getOption('configuration');
        $useDefault = false;
        if (null === $configFile || false === $configFile) {
            $useDefault = true;
        }
        $cwd = getcwd();
        $locator = new FileLocator([
            $cwd.DIRECTORY_SEPARATOR,
        ]);
        if (!$useDefault) {
            $path = $locator->locate($configFile, $cwd, $first = true);
        } else {
            try {
                $path = $locator->locate('clik.yaml', $cwd, $first = true);
            } catch (\InvalidArgumentException $exception) {
                throw $exception;
            }
        }
        $this->config = Yaml::parseFile($path);
    }
}
