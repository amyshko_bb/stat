<?php

declare(strict_types=1);

namespace Clik\Command;

use Clik\Exception\InvalidConfigurationException;
use Clik\Mailer\MailerHelper;
use Clik\Model\Period;
use Clik\Repository\MiddlewareRepository;
use Clik\Repository\NstatsRepository;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as ReaderXlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as WriterXlsx;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class VodCommand extends AbstractCommand
{
    const DEFAULT_TEMPLATE = '/../../resources/vod_report_template.xlsx';

    const DEFAULT_CATEGORY = 'Мультфильм';

    protected static $defaultName = 'report:monthly:vod';

    protected function configure()
    {
        parent::configure();
        $this
            ->setDescription('Generate vod statistic.')
            ->addOption('receivers', 'r', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED)
            ->addOption('month', 'm', InputOption::VALUE_OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadConfig($input);
        $receivers = $input->getOption('receivers');
        if (!is_array($receivers) || 0 === count($receivers)) {
            throw new InvalidConfigurationException();
        }
        $month = $input->getOption('month');
        $period = new Period(null === $month ? null : $month);
        $middleware = new MiddlewareRepository($this->config['databases']['middleware']);
        $files = $middleware->findFilmIdsByCategory(self::DEFAULT_CATEGORY);
        $statistic = new NstatsRepository($this->config['databases']['statistic']);
        $mailer = new MailerHelper($this->config['mailer']);
        $mailer->sendEmail([
            'subject' => sprintf('VOD REPORT %s', date('m-Y', strtotime($period->getStart()))),
            'recipient' => $receivers,
            'body' => '',
            'attachment' => $this->makeReport($period, [
                'vod' => [
                    'all' => $statistic->countVod($period),
                    'without_catygory' => $statistic->countVod($period, $files),
                ],
                'vod_advert' => [
                    'all' => $statistic->countVodAdvert($period),
                    'without_catygory' => $statistic->countVodAdvert($period, $files),
                ],
            ]),
        ]);
    }

    protected function getTemplateFilename()
    {
        return __DIR__.self::DEFAULT_TEMPLATE;
    }

    private function makeReport(Period $period, array $data)
    {
        if (!file_exists($this->getTemplateFilename())) {
            throw new InvalidConfigurationException();
        }
        $spreadsheet = (new ReaderXlsx())->load($this->getTemplateFilename());
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A4', (new \DateTime($period->getStart()))->format('d.m.Y'));
        $sheet->setCellValue('B4', (new \DateTime($period->getEnd()))->format('d.m.Y'));
        $sheet->setCellValue('C4', $data['vod']['all']);
        $sheet->setCellValue('D4', $data['vod']['without_catygory']);
        $sheet->setCellValue('A9', (new \DateTime($period->getStart()))->format('d.m.Y'));
        $sheet->setCellValue('B9', (new \DateTime($period->getEnd()))->format('d.m.Y'));
        $sheet->setCellValue('C9', $data['vod_advert']['all']);
        $sheet->setCellValue('D9', $data['vod_advert']['without_catygory']);
        $writer = new WriterXlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');

        return ob_get_clean();
    }
}
