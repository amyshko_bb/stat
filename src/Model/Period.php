<?php

namespace Clik\Model;

final class Period
{
    private $start;

    private $end;

    public function __construct(string $month = null)
    {
        if (null === $month) {
            $this->start = date('Y-m-01 00:00:00', strtotime('-1 month'));
            $this->end = date('Y-m-t 23:59:59', strtotime('-1 month'));
        }
    }

    public function getStart(): string
    {
        return $this->start;
    }

    public function getEnd(): string
    {
        return $this->end;
    }
}
