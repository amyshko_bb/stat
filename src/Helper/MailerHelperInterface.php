<?php

namespace Clik\Mailer;

interface MailerHelperInterface
{
    public function sendEmail(array $options): int;
}
