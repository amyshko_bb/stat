<?php

namespace Clik\Mailer;

use Symfony\Component\OptionsResolver\OptionsResolver;

final class MailerHelper implements MailerHelperInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(array $configuration)
    {
        $this->mailer = new \Swift_Mailer(SwiftmailerTransportFactory::createTransport($configuration));
    }

    public function sendEmail(array $options): int
    {
        $message = $this->createMessage($options);

        try {
            return $this->mailer->send($message);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function createMessage(array $options): \Swift_Message
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $options = $resolver->resolve($options);

        $message = (new \Swift_Message())
            ->setSubject($options['subject'])
            ->setFrom($options['from'])
            ->setTo($options['recipient'])
            ->setBody($options['body'], 'text/html');

        if (null !== $options['attachment']) {
            $message->attach(new \Swift_Attachment($options['attachment'], 'vod_stat.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
        }

        return $message;
    }

    private function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            'recipient',
            'subject',
            'body',
        ]);
        $resolver->setDefault('from', ['noreply@kartina.tv' => 'Kartina.TV']);
        $resolver->setAllowedTypes('recipient', ['string', 'array']);
        $resolver->setAllowedTypes('from', ['string', 'array']);
        $resolver->setAllowedTypes('subject', ['string']);
        $resolver->setAllowedTypes('body', ['string']);
        $resolver->setDefault('attachment', null);
        $resolver->setAllowedTypes('attachment', ['string', 'null']);
    }
}
