<?php

namespace Clik\Mailer;

final class SwiftmailerTransportFactory
{
    public static function createTransport(array $options): \Swift_SmtpTransport
    {
        $options = static::resolveOptions($options);
        self::validateConfig($options);
        if ('smtp' !== $options['transport']) {
            throw new \InvalidArgumentException(sprintf('Not a Swiftmailer transport: %s.', $options['transport']));
        }

        return (new \Swift_SmtpTransport($options['host'], $options['port']))
            ->setUsername($options['username'])
            ->setPassword($options['password'])
            ->setEncryption($options['encryption'])
            ->setTimeout($options['timeout']);
    }

    private static function resolveOptions(array $options): array
    {
        $options += [
            'transport' => null,
            'username' => null,
            'password' => null,
            'host' => null,
            'port' => null,
            'timeout' => null,
            'encryption' => null,
        ];
        if (isset($options['url'])) {
            $parts = parse_url($options['url']);
            if (isset($parts['scheme'])) {
                $options['transport'] = $parts['scheme'];
            }
            if (isset($parts['user'])) {
                $options['username'] = $parts['user'];
            }
            if (isset($parts['pass'])) {
                $options['password'] = $parts['pass'];
            }
            if (isset($parts['host'])) {
                $options['host'] = $parts['host'];
            }
            if (isset($parts['port'])) {
                $options['port'] = $parts['port'];
            }
            if (isset($parts['query'])) {
                parse_str($parts['query'], $query);
                foreach ($options as $key => $value) {
                    if (isset($query[$key]) && '' !== $query[$key]) {
                        $options[$key] = $query[$key];
                    }
                }
            }
        }

        return $options;
    }

    private static function validateConfig($options)
    {
        if (!in_array($options['encryption'], ['tls', 'ssl', null], true)) {
            throw new \InvalidArgumentException(sprintf('The %s encryption is not supported', $options['encryption']));
        }
    }
}
